const http = require("./app");

module.exports = (http) =>{
    const io = require('socket.io')(http)

    io.on('connection', (socket)=>{
        console.log('user conected')// si alguien entra notifica
        //evento que escuche
        socket.on('chat-message',(msg)=>{
            //al mismo tiempo se envia el mensaje a todos los clients conectados
            io.emit('chat-message',msg)
        })

        socket.on('disconnect',()=>{
            console.log('user disconected')
        })
    })
}